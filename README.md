## Sinclair BASIC line-length calculator. 

## Description
A tool for counting the length of zxspectrum BASIC lines in characters. This tool was intended to aid in calculating the length of lines for Basic 10 Liner contest.  

The document will substitute tokens by a single character. String control codes will be substituted by the number of characters they really ocuppy in the string. Both, bas2tap and BasinC control character format is supported. 

**The author is not affiliated nor represent the the competition in any form. The output of this document is not guaranteed to comply with the rules. Please, make sure to check the rules in the official website https://gkanold.wixsite.com/homeputerium**

## Usage
1. Open the document and make sure macros are enabled.
2. Clear the previous text or just click on "clear" button.
3. Paste your code without any formatting. Use paste special if required.
4. Click "count characters". An options dialog will open.
5. Select the desired options and format and press "OK". 

Note. Clicking a secound time on "count characters" will corrupt the result. To reprocess the code, clear the document content and repeat the process from the start.

## Authors and acknowledgment
Created by Moises Garin.

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 